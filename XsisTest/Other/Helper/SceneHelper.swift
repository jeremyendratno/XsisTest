//
//  ScanaHelper.swift
//  XsisTest
//
//  Created by Jeremy Endratno on 12/5/22.
//

import Foundation
import UIKit

class SceneHelper {
    static func rootSetup(scene: UIScene) -> UIWindow? {
        let initialView: UINavigationController = DarkModeNavigationController(rootViewController: HomeViewController())
        
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            window.rootViewController = initialView
            window.makeKeyAndVisible()
            return window
        } else {
            return nil
        }
    }
}

class DarkModeNavigationController: UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
