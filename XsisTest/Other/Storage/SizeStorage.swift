//
//  SizeStorage.swift
//  XsisTest
//
//  Created by Jeremy Endratno on 12/5/22.
//

import Foundation
import UIKit

class SizeStorage {
    static let screenSize = UIScreen.main.bounds.size
    static let screenWidth = UIScreen.main.bounds.width
    static let screenHeight = UIScreen.main.bounds.height
    static let halfScreenWidth = UIScreen.main.bounds.width / 2
    static let halfScreenHeight = UIScreen.main.bounds.height / 2
    static let centerPoint = CGPoint(x: halfScreenWidth, y: halfScreenHeight)
}
