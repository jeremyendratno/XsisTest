//
//  UISearchBar.swift
//  XsisTest
//
//  Created by Jeremy Endratno on 12/5/22.
//

import Foundation
import UIKit

extension UISearchBar {
    func setColor(color: UIColor = .white) {
        if let textField = value(forKey: "searchField") as? UITextField {
            textField.backgroundColor = color
        }
    }
}
