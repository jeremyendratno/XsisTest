//
//  UICollectionView.swift
//  XsisTest
//
//  Created by Jeremy Endratno on 12/5/22.
//

import Foundation

import UIKit

extension UICollectionView {
    func getViewedIndex(isHorizontal: Bool = true) -> IndexPath? {
        var point = CGPoint(x: 0, y: 0)
        
        if isHorizontal {
            point = CGPoint(x: SizeStorage.halfScreenWidth + contentOffset.x, y: frame.maxY / 2)
        } else {
            point = CGPoint(x: SizeStorage.halfScreenWidth, y: SizeStorage.screenHeight + contentOffset.y)
        }
        
        let index = indexPathForItem(at: point)
        return index
    }
}
