//
//  UIView.swift
//  XsisTest
//
//  Created by Jeremy Endratno on 12/5/22.
//

import Foundation
import UIKit

extension UIView {
    func cornerRadius(_ size: CGFloat) {
        self.layer.cornerRadius = size
    }
    
    func circleCornerRadius() {
        DispatchQueue.main.async {
            self.layer.cornerRadius = self.frame.height / 2
        }
    }
    
    func addTapGesture(target: UIViewController, action: Selector?) {
        let tap = UITapGestureRecognizer(target: target, action: action)
        addGestureRecognizer(tap)
    }
}
