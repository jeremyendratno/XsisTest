//
//  MovieService.swift
//  XsisTest
//
//  Created by Jeremy Endratno on 12/5/22.
//

import Foundation
import Alamofire

class MovieService {
    static func getMovies(search: String = "avenger", onDone: @escaping (RequestResult<SearchResponse>) -> Void) {
        let url = "https://www.omdbapi.com"
        let parameter: Parameters = [
            "s": search,
            "apikey": "feecadf3"
        ]
        
        BaseService.request(url: url, parameter: parameter) { response in
            onDone(response)
        }
    }
    
    static func getMovie(id: String, isLongPlot: Bool = false, onDone: @escaping (RequestResult<Movie>) -> Void) {
        let url = "https://www.omdbapi.com"
        let parameter: Parameters = [
            "i": id,
            "apikey": "feecadf3",
            "plot": isLongPlot ? "full" : "short"
        ]
        
        BaseService.request(url: url, parameter: parameter) { response in
            onDone(response)
        }
    }
}
