//
//  BaseService.swift
//  XsisTest
//
//  Created by Jeremy Endratno on 12/5/22.
//

import Foundation
import Alamofire

enum RequestFailure {
    case errorOccured
    case cancelled
    case dataNil
    case failedToDecode
    case urlNotFound
    case unknown
}

enum RequestResult<T> {
    case success(T)
    case failure(RequestFailure)
}

class BaseService {
    static func request<T: Codable>(url: String, method: HTTPMethod = .get, parameter: Parameters = [:], completionHandler: @escaping (RequestResult<T>) -> Void) {
        guard let url = URL(string: url) else {
            completionHandler(.failure(.urlNotFound))
            print("Request \(url) failed, url is not found")
            return
        }
        
        AF.request(url, method: method, parameters: parameter).validate().response() { response in
            if let error = response.error {
                if error.localizedDescription == "URLSessionTask failed with error: cancelled" {
                    completionHandler(.failure(.cancelled))
                    print("Request \(url) failed, has been cancelled")
                } else {
                    completionHandler(.failure(.errorOccured))
                    print("Request \(url) failed, error occurred: \(error.localizedDescription)")
                }
                
                return
            }
            
            guard let data = response.data else {
                completionHandler(.failure(.dataNil))
                print("Request \(url) failed, response data found nil")
                return
            }
            
            do {
                let decodedData = try JSONDecoder().decode(T.self, from: data)
                completionHandler(.success(decodedData))
                print("Request \(url) success")
            } catch {
                completionHandler(.failure(.failedToDecode))
                print("Request \(url) failed, failed to decode")
            }
        }
    }
}
