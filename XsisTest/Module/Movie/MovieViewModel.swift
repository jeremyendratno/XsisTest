//
//  MovieViewModel.swift
//  XsisTest
//
//  Created by Jeremy Endratno on 12/7/22.
//

import Foundation

protocol MovieDelegate: AnyObject {
    func onSuccessMovie()
    func onSuccessLatest()
}

class MovieViewModel {
    weak var delegate: MovieDelegate?
    var id: String = ""
    var movie: Movie?
    var latestMovies: [Movie] = []
    
    func requestMovie() {
        MovieService.getMovie(id: id, isLongPlot: true) { response in
            switch response {
            case .success(let data):
                self.movie = data
                self.delegate?.onSuccessMovie()
            case .failure(_):
                break
            }
        }
    }
    
    func requestLatestMovies() {
        MovieService.getMovies(search: "superman") { response in
            switch response {
            case .success(let data):
                self.latestMovies = data.Search
                self.delegate?.onSuccessLatest()
            case .failure(_):
                break
            }
        }
    }
}
