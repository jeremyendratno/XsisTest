//
//  MovieViewController.swift
//  XsisTest
//
//  Created by Jeremy Endratno on 12/7/22.
//

import UIKit

class MovieViewController: BaseViewController, MovieDelegate {
    var viewModel = MovieViewModel()
    
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieDescriptionLabel: UILabel!
    @IBOutlet weak var latestCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        viewModel.requestMovie()
        viewModel.requestLatestMovies()
        
        latestCollectionView.delegate = self
        latestCollectionView.dataSource = self
        latestCollectionView.register(UINib(nibName: "MovieCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "latest")
    }
    
    func onSuccessMovie() {
        guard let movie = viewModel.movie else { return }
        
        if let imageUrl = URL(string: movie.Poster) {
            movieImageView.af.setImage(withURL: imageUrl)
        }
        
        movieTitleLabel.text = movie.Title
        movieDescriptionLabel.text = movie.Plot
    }
    
    func onSuccessLatest() {
        latestCollectionView.reloadData()
    }
}

extension MovieViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.latestMovies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "latest", for: indexPath) as! MovieCollectionViewCell
        cell.setup(movie: viewModel.latestMovies[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 200, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let movieVC = MovieViewController()
        movieVC.viewModel.id = viewModel.latestMovies[indexPath.row].imdbID
        present(movieVC, animated: true)
    }
}
