//
//  MovieTallCollectionViewCell.swift
//  XsisTest
//
//  Created by Jeremy Endratno on 12/6/22.
//

import UIKit

class MovieTallCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleImageView: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cornerRadius(20)
    }
    
    func setup(movie: Movie) {
        if let imageUrl = URL(string: movie.Poster) {
            movieImageView.af.setImage(withURL: imageUrl)
        } else {
            movieImageView.image = nil
        }
        
        titleImageView.text = movie.Title
    }
}
