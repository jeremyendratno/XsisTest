//
//  MovieFeaturedCollectionViewCell.swift
//  XsisTest
//
//  Created by Jeremy Endratno on 12/5/22.
//

import UIKit
import AlamofireImage

class MovieFeaturedCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cornerRadius(20)
    }
    
    func setup(movie: Movie) {
        if let posterUrl = URL(string: movie.Poster) {
            movieImageView.af.setImage(withURL: posterUrl)
        }
        
        titleLabel.text = movie.Title
        descriptionLabel.text = movie.Plot
    }
}
