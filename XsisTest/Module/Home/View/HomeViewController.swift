//
//  HomeViewController.swift
//  XsisTest
//
//  Created by Jeremy Endratno on 12/5/22.
//

import UIKit

class HomeViewController: BaseViewController, HomeDelegate {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var featuredCollectionView: UICollectionView!
    @IBOutlet weak var featuredPageControl: UIPageControl!
    @IBOutlet weak var latestCollectionView: UICollectionView!
    @IBOutlet weak var actionCollectionView: UICollectionView!
    
    let viewModel = HomeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        viewModel.requestFeaturedMovie()
        viewModel.requestLatestMovie()
        viewModel.requestActionMovie()
        
        featuredCollectionView.delegate = self
        featuredCollectionView.dataSource = self
        featuredCollectionView.register(UINib(nibName: "MovieFeaturedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "featured")
        
        latestCollectionView.delegate = self
        latestCollectionView.dataSource = self
        latestCollectionView.register(UINib(nibName: "MovieCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "latest")
        
        actionCollectionView.delegate = self
        actionCollectionView.dataSource = self
        actionCollectionView.register(UINib(nibName: "MovieTallCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "action")
    }
    
    @IBAction func searchButton(_ sender: Any) {
        navigationController?.pushViewController(SearchViewController(), animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == featuredCollectionView, let index = featuredCollectionView.getViewedIndex() {
            featuredPageControl.currentPage = index.row
        }
    }
    
    func featuredPageControlSetup() {
        featuredPageControl.numberOfPages = viewModel.featuredMovies.count
    }
    
    func onSuccessFeatured() {
        featuredCollectionView.reloadData()
        featuredPageControlSetup()
    }
    
    func onSuccessLatest() {
        latestCollectionView.reloadData()
    }
    
    func onSuccessAction() {
        actionCollectionView.reloadData()
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == featuredCollectionView {
            return viewModel.featuredMovies.count
        } else if collectionView == latestCollectionView {
            return viewModel.actionMovies.count
        } else {
            return viewModel.latestMovies.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == featuredCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "featured", for: indexPath) as! MovieFeaturedCollectionViewCell
            cell.setup(movie: viewModel.featuredMovies[indexPath.row])
            return cell
        } else if collectionView == actionCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "action", for: indexPath) as! MovieTallCollectionViewCell
            cell.setup(movie: viewModel.actionMovies[indexPath.row])
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "latest", for: indexPath) as! MovieCollectionViewCell
            cell.setup(movie: viewModel.latestMovies[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == featuredCollectionView {
            let width = SizeStorage.screenWidth - 40
            return CGSize(width: width, height: 150)
        } else if collectionView == actionCollectionView {
            return CGSize(width: 150, height: 200)
        } else {
            return CGSize(width: 200, height: 100)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let movieVC = MovieViewController()
        
        if collectionView == featuredCollectionView {
            movieVC.viewModel.id = viewModel.featuredMovies[indexPath.row].imdbID
        } else if collectionView == latestCollectionView {
            movieVC.viewModel.id = viewModel.latestMovies[indexPath.row].imdbID
        } else if collectionView == actionCollectionView {
            movieVC.viewModel.id = viewModel.actionMovies[indexPath.row].imdbID
        }
        
        present(movieVC, animated: true)
    }
}
