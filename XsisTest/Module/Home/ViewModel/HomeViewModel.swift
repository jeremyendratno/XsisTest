//
//  HomeViewModel.swift
//  XsisTest
//
//  Created by Jeremy Endratno on 12/5/22.
//

import Foundation

protocol HomeDelegate: AnyObject {
    func onSuccessFeatured()
    func onSuccessLatest()
    func onSuccessAction()
}

class HomeViewModel {
    weak var delegate: HomeDelegate?
    var featuredMovies: [Movie] = []
    var latestMovies: [Movie] = []
    var actionMovies: [Movie] = []
    
    func requestFeaturedMovie() {
        MovieService.getMovies() { response in
            switch response {
            case .success(let data):
                self.featuredMovies = data.Search
                self.fillPlot()
            case .failure(_):
                break
            }
        }
    }
    
    func fillPlot() {
        if let index = featuredMovies.firstIndex(where: { $0.Plot == nil }) {
            MovieService.getMovie(id: featuredMovies[index].imdbID) { response in
                switch response {
                case .success(let data):
                    self.featuredMovies[index].Plot = data.Plot
                    self.fillPlot()
                case .failure(_):
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.fillPlot()
                    }
                }
            }
        } else {
            delegate?.onSuccessFeatured()
        }
    }
    
    func requestLatestMovie() {
        MovieService.getMovies(search: "superman") { response in
            switch response {
            case .success(let data):
                self.latestMovies = data.Search
                self.delegate?.onSuccessLatest()
            case .failure(_):
                break
            }
        }
    }
    
    func requestActionMovie() {
        MovieService.getMovies(search: "batman") { response in
            switch response {
            case .success(let data):
                self.actionMovies = data.Search
                self.delegate?.onSuccessAction()
            case .failure(_):
                break
            }
        }
    }
}
