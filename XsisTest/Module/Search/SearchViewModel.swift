//
//  SearchViewModel.swift
//  XsisTest
//
//  Created by Jeremy Endratno on 12/6/22.
//

import Foundation

protocol SearchDelegate: AnyObject {
    func onSuccessSearch()
}

class SearchViewModel {
    weak var delegate: SearchDelegate?
    var movies: [Movie] = []
    
    func requestSearch(search: String) {
        MovieService.getMovies(search: search) { response in
            switch response {
            case .success(let data):
                self.movies = data.Search
                self.delegate?.onSuccessSearch()
            case .failure(_):
                break
            }
        }
    }
}
