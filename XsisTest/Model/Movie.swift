//
//  Movie.swift
//  XsisTest
//
//  Created by Jeremy Endratno on 12/5/22.
//

import Foundation

struct Movie: Codable {
    var imdbID: String
    var Title: String = "No Title"
    var Poster: String = "No Poster"
    var Plot: String? = nil
}
