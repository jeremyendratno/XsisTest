//
//  BaseModel.swift
//  XsisTest
//
//  Created by Jeremy Endratno on 12/5/22.
//

import Foundation

struct SearchResponse: Codable {
    var Search: [Movie]
}
