//
//  BaseViewController.swift
//  XsisTest
//
//  Created by Jeremy Endratno on 12/5/22.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
    }
}
